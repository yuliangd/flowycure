#ifdef ARDUINO_SAMD_NANO_33_IOT
#include "WiFiManager_Lite.h"

WiFiManager_Lite::WiFiManager_Lite()
{
  this->newSsid = "";
  this->newPass = "";
  this->newCredentialReady = false;
}

int WiFiManager_Lite::connectWifi(String _ssid, String _pass)
{ // attempt to connect to Wifi network:
  static unsigned long time_started_scan = millis();
  int res;
  bool timeout_flag = false;
  Serial.print("Attempting to connect to WPA SSID: ");
  Serial.println(_ssid);
  do
  {
    res = WiFi.begin(_ssid.c_str(), _pass.c_str());
    // failed, retry
    Serial.print(".");
    if (millis() - time_started_scan > 10000)
    {
      Serial.println("Failed to connect.");
      timeout_flag = true;
    }
    delay(5000);
  } while ((res != WL_CONNECTED) && !timeout_flag);

  if (WiFi.status() == WL_CONNECTED)
  {
    Serial.println("");
    Serial.println("WiFi Connected!");
    Serial.println(WiFi.localIP());
    Serial.println();
  }
  return res;
}

void WiFiManager_Lite::ap_prepare()
{
  this->newCredentialReady = false;
  this->receivedString = "";
}

void WiFiManager_Lite::httpHandler(WiFiServer &_ap_server)
{

  static int status;
  // compare the previous status to the current status
  if (status != WiFi.status())
  {
    // it has changed update the variable
    status = WiFi.status();

    if (status == WL_AP_CONNECTED)
    {
      // a device has connected to the AP
      Serial.println("Device connected to AP");
    }
    else
    {
      // a device has disconnected from the AP, and we are back in listening mode
      Serial.println("Device disconnected from AP");
    }
  }

  WiFiClient client = _ap_server.available(); // listen for incoming clients
  if (client)
  {                               // if you get a client,
    Serial.println("new client"); // print a message out the serial port
    String currentLine = "";

    while (client.connected())
    { // loop while the client's connected
      if (client.available())
      {                         // if there's bytes to read from the client,
        char c = client.read(); // read a byte, then
        Serial.write(c);        // print it out the serial monitor
        if (c == '\n')
        { // if the byte is a newline character

          if (currentLine.length() == 0)
          {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line:
            client.println("HTTP/1.1 200 OK");
            client.println("Content-type:text/html");
            client.println();

            // the content of the HTTP response follows the header:
            client.print(HTTP_HEADER);
            client.print(HTTP_STYLE);
            client.print(HTTP_SCRIPT);
            client.print(HTTP_HEADER_END);
            client.print(HTTP_FORM_START);
            client.print(HTTP_FORM_END);
            client.print(HTTP_END);

            // The HTTP response ends with another blank line:
            client.println();
            // break out of the while loop:
            break;
          }
          else
          { // if you got a newline, then clear currentLine:
            int pos = currentLine.indexOf("wifisave?");
            if (pos > 0)
            {
              this->receivedString = currentLine.substring(pos + 11);
            }
            currentLine = "";
          }
        }
        else if (c != '\r')
        {                   // if you got anything else but a carriage return character,
          currentLine += c; // add it to the end of the currentLine
        }
      }
    }
    // close the connection:
    client.stop();
    Serial.println("client disconnected");
  }
}

bool WiFiManager_Lite::isNewCredentialReady()
{
  if (this->receivedString.length() > 6)
  {
    int pos = 0;
    if (this->receivedString.indexOf("%") > 0) // cleaning up some encoded string from the URL
    {
      this->receivedString.replace("%21", "!");
      this->receivedString.replace("%23", "#");
      this->receivedString.replace("%24", "$");
      this->receivedString.replace("%25", "%");
      this->receivedString.replace("%26", "&");
      this->receivedString.replace("%28", "(");
      this->receivedString.replace("%29", ")");
      this->receivedString.replace("%2A", "*");
      this->receivedString.replace("%2D", "-");
    }
    pos = this->receivedString.indexOf("&p=");
    if (pos > 0)
    {
      newSsid = this->receivedString.substring(0, pos);
      newPass = this->receivedString.substring(pos + 3);
      if ((newSsid.length() > 3) && (newPass.length() > 4))
      {
        this->newCredentialReady = true;
        Serial.println("New SSID-" + newSsid);
        Serial.println("New Pass-" + newPass);
      }
    }
  }
  if (!newCredentialReady)
  {
    this->receivedString = ""; // Reset the received string if invalid
  }
  return this->newCredentialReady;
};

String WiFiManager_Lite::getNewSsid() { return this->newSsid; };
String WiFiManager_Lite::getNewPass() { return this->newPass; };

#endif