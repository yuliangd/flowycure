#include "RecipeHandler.h"

RecipeHandler::RecipeHandler(int _dof = 5)
{
    this->_n = _dof;
}

String RecipeHandler::getDesc(int _i)
{
    return this->curvar[_i].desc;
};

String RecipeHandler::getDescValueUnit(int _i)
{
    return this->curvar[_i].desc + String(this->curvar[_i].sp_temp) + this->curvar[_i].unit;
};

int RecipeHandler::getSetpoint(int _i)
{
    return this->curvar[_i].setpoint;
};

bool RecipeHandler::setSetpoint(int _i, int _sp)
{
    if (_sp >= this->curvar[_i].sp_min && _sp <= this->curvar[_i].sp_max)
    {
        this->curvar[_i].setpoint = _sp;
        return true;
    }
    else
        return false;
};

int RecipeHandler::getSpTemporary(int _i)
{
    return this->curvar[_i].sp_temp;
};

char RecipeHandler::getUnit(int _i)
{
    return this->curvar[_i].unit;
};

void RecipeHandler::initAllTemp()
{
    for (int i = 0; i < this->_n; i++)
    {
        this->curvar[i].sp_temp = this->curvar[i].setpoint;
    }
}

void RecipeHandler::applyAllTemp()
{
    for (int i = 0; i < this->_n; i++)
    {
        this->curvar[i].setpoint = this->curvar[i].sp_temp;
    }
}

void RecipeHandler::printSerial()
{
    if (Serial)
    {
        for (int i = 0; i < _n; i++)
        {
            Serial.print(curvar[i].desc);
            Serial.print(curvar[i].setpoint);
            Serial.println(curvar[i].unit);
        }
    }
}

void RecipeHandler::changeSpTemporary(int _i, int _delta)
{
    this->curvar[_i].sp_temp = max(min((this->curvar[_i].sp_temp + _delta * this->curvar[_i].change_inc), this->curvar[_i].sp_max), this->curvar[_i].sp_min);
}
