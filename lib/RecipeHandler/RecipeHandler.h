#ifndef RECIPEHANDLER_H
#define RECIPEHANDLER_H

#include <Arduino.h>

class RecipeHandler
{
private:
    struct CuringVariable
    {
        String desc;
        int setpoint;
        int sp_min;
        int sp_max;
        char unit;
        int change_inc;
        int sp_temp;
    };
    int _n;
    CuringVariable curvar[7] = {{"B.Interval ", 24, 6, 48, 'h', 6, 0},
                                {"B.Duration ", 5, 5, 240, 'm', 5, 0},
                                {"Early B.RH>", 72, 50, 100, '%', 1, 0},
                                {"Abort B.RH<", 20, 0, 70, '%', 1, 0},
                                {"Done @ RH <", 61, 35, 80, '%', 1, 0},
                                {"Check Dur. ", 90, 5, 480, 'm', 5, 0},
                                {"Days Limit ", 48, 1, 48, 'd', 1, 0}};

public:
    RecipeHandler(int _dof);
    String getDesc(int _i);
    String getDescValueUnit(int _i);
    int getSetpoint(int _i);
    int getSpTemporary(int _i);
    char getUnit(int _i);
    bool setSetpoint(int _i, int _sp);
    void initAllTemp();
    void applyAllTemp();
    void printSerial();
    void changeSpTemporary(int _i, int _delta);
};

#endif
