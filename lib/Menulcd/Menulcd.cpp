#include "Menulcd.h"

Menulcd::Menulcd(int _of, int _mu)
{
    this->_itemcount = 0;
    this->_multiplier = _mu;
    this->_offset = _of;
    this->_lcdrows = 2;
    this->_position = 0;
};

void Menulcd::addItem(String _item)
{
    _items[this->_itemcount] = _item;
    if (Serial)
    {
        Serial.print("Menu item '" + _item + "' added at Position #");
        Serial.print(this->_itemcount);
        Serial.print(". Global Code #");
        Serial.println(this->posToCode(this->_itemcount));
    }
    this->_itemcount++;
}

void Menulcd::setPosition(int _p)
{
    this->_position = _p;
    this->assertPos();
    if (Serial)
    {
        Serial.print("Menu position set to ");
        Serial.print(this->_position);
        Serial.print(". Global Code #");
        Serial.println(this->getPosCode());
    }
}

int Menulcd::getPosition()
{
    return this->_position;
}

int Menulcd::getPosCode()
{
    return this->posToCode(this->_position);
}

String Menulcd::getCurItem()
{
    return this->_items[this->_position];
}

void Menulcd::movePos(int _inc)
{
    this->_position += _inc;
    this->assertPos();
}

void Menulcd::assertPos()
{
    if (this->_position < 0)
    {
        this->_position += this->_itemcount * 5;
    }
    this->_position = this->_position % this->_itemcount;
}

int Menulcd::posToCode(int _p)
{
    return _p * this->_multiplier + this->_offset;
}

#if DFR_1602_LCD // defined in "DefinePin.h"
void Menulcd::updateLCD(DFRobot_LCD &_lcd)
#else
void Menulcd::updateLCD(LiquidCrystal_I2C &_lcd)
#endif
{
    _lcd.clear();
    const char cursor_char = char(255);
    const int _xo = 1;
    if (this->_itemcount > this->_lcdrows) // scrolling if items more than lcd rows
    {
        for (int i = 0; i < this->_lcdrows; i++)
        {
            _lcd.setCursor(_xo, i);
            _lcd.print(this->_items[(this->_position + i) % this->_itemcount]);
        }
        _lcd.setCursor(0, 0);
        _lcd.print(cursor_char);
    }
    else // fixed position otherwise
    {
        for (int i = 0; i < this->_itemcount; i++)
        {
            _lcd.setCursor(_xo, i);
            _lcd.print(this->_items[i]);
        }
        _lcd.setCursor(0, this->_position);
        _lcd.print(cursor_char);
    }
}

void Menulcd::updateItem(int _p, String _item)
{
    this->_items[_p] = _item;
}