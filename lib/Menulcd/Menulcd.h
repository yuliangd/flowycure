#ifndef MENULCD_H
#define MENULCD_H

#include <Arduino.h>
#include <Wire.h>
#include "../../src/DefinePin.h"

#if DFR_1602_LCD // defined in "DefinePin.h"
#include "DFRobot_LCD.h"
#else
#include <LiquidCrystal_I2C.h> // Use <LiquidCrystal_I2C.h> alternatively in case
#endif

class Menulcd
{
private:
    /* data */
    int _multiplier;
    int _offset;
    int _position;
    int _itemcount;
    int _lcdrows;
    String _items[15];
    void assertPos();
    int posToCode(int _p);

public:
    Menulcd(int _of = 0, int _mu = 1);
    void addItem(String _item);
    void updateItem(int _p, String _item);
    void setPosition(int _p);
    int getPosition();
    int getPosCode();
    String getCurItem();
    void movePos(int _inc);
//
#if DFR_1602_LCD // defined in "DefinePin.h"
    void updateLCD(DFRobot_LCD &_lcd);
#else
    void updateLCD(LiquidCrystal_I2C &_lcd);
#endif
};

#endif