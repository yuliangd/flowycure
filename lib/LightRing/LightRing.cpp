#include "LightRing.h"

LightRing::LightRing(Adafruit_NeoPixel &_p, unsigned int _pixelcount)
{
    this->p = _p;
    this->_nopixels = _pixelcount;
    this->_step_msec = 250; // each step in ms
}

void LightRing::begin()
{
    this->p.begin();
    this->p.setBrightness(255);
}

void LightRing::showRotate()
{
    for (int i = 1; i < this->_nopixels; i++)
    {
        this->p.setPixelColor(i, this->p.Color(0, 0, 255));
        this->p.show();
        delay(60);
    }
    delay(1200);
    for (int i = 1; i < this->_nopixels; i++)
    {
        this->p.setPixelColor(i, this->p.Color(0, 0, 0));
        this->p.show();
        delay(60);
    }
}

void LightRing::showPattern(int _s, int _hue_modifier)
{
    static unsigned long timer_target = 0;
    static unsigned int timing_step = 0;

    if (millis() > timer_target)
    {
        timer_target = millis() + this->_step_msec;
        int timing_step = (millis() / this->_step_msec) % SEQ_STEPS;
        const unsigned int hue_dry_yellow = 65535 / 360 * 50,
                           hue_wet_blue = 65535 / 360 * 260,
                           saturation = 255;
        unsigned int val = this->valmatrix[_s][timing_step];
        unsigned int hue = map(_hue_modifier, 40, 100, hue_dry_yellow, hue_wet_blue);
        uint32_t rgb_color = this->p.ColorHSV(hue, saturation, val);
        for (int i = 1; i < this->_nopixels; i++)
        {
            // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
            int bit_on = bitRead(this->lightcode[_s][timing_step], i % 3);
            this->p.setPixelColor(i, bit_on * rgb_color); // Moderately bright green color.
        }
        this->p.show(); // This sends the updated pixel color to the hardware.
        timing_step = (timing_step + 1) % SEQ_STEPS;
    }
}

void LightRing::showColorFlash(uint32_t _color, int _dur)
{
    this->p.fill(_color);
    this->p.show();
    delay(_dur);
    this->p.clear();
    this->p.show();
}