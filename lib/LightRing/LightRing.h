#ifndef LIGHTRING_H
#define LIGHTRING_H

#define SEQ_STEPS 9

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

class LightRing
{
private:
    int _nopixels = 7;
    int _step_msec;
    Adafruit_NeoPixel p;
    // LED ring pattern sequence.
    const int lightcode[5][SEQ_STEPS] = {
        {0b111, 0b111, 0b111, 0b111, 0b111, 0b111, 0b111, 0b111, 0b111}, // Stage 0, 9 steps, 3 pixels
        {0b001, 0b000, 0b000, 0b010, 0b000, 0b000, 0b100, 0b000, 0b000}, // Stage 1, etc...
        {0b111, 0b111, 0b111, 0b111, 0b111, 0b111, 0b111, 0b111, 0b111},
        {0b001, 0b001, 0b011, 0b010, 0b010, 0b110, 0b100, 0b100, 0b101},
        {0b000, 0b000, 0b111, 0b000, 0b000, 0b111, 0b000, 0b000, 0b111}};

    const int valmatrix[5][SEQ_STEPS] = {
        {64, 64, 64, 255, 64, 64, 64, 255, 255},
        {64, 64, 255, 64, 64, 255, 64, 64, 255},
        {24, 87, 163, 220, 251, 251, 220, 163, 87}, // [int(255*math.sin(math.pi*i/9)) for i in range(9)]
        {255, 255, 128, 255, 255, 128, 255, 255, 128},
        {255, 255, 128, 255, 255, 255, 255, 255, 255},
    };

public:
    LightRing(Adafruit_NeoPixel &_p, unsigned int _pixelcount);
    void begin();
    void showRotate();
    void showPattern(int _s, int _hue_modifier);
    void showColorFlash(uint32_t _color, int _dur);
};

#endif