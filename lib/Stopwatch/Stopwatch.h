/*
  A stopwatch / timetracker.
  2021 Copyright (c)
  Author: 'DyLaron' Aaron Yuliang Deng
  Date: MAY-26-2021
  Latest Update: JUL-14-2021
*/

#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <Arduino.h>

class Stopwatch
{
private:
  unsigned long started_time, _paused_time;
  unsigned long timer_sec;
  int _status;
  String status_strings[3]= {"STANDBY", "ACTIVE", "PAUSED"};

public:
  Stopwatch();
  void start();
  void pause();
  void unpause();
  void reset();
  void setTimerSec(unsigned long _s);
  void startTimerSec(unsigned long _s);
  unsigned long getElapsedMs();
  unsigned long getElapsedSec();
  unsigned int getElapsedMin();
  unsigned int getElapsedHour();
  unsigned int getElapsedDay();
  unsigned long getRemainingSec();
  String secToStr(unsigned long _s);
  String getElapsedString();
  String getRemainingString();
  String getStatusString();
  bool checkForSec(unsigned long _s);
  bool isStandby();
  bool isPaused();
  bool isRunning();
  bool isTimerDone();
  bool isTimerPending();
  bool checkPulse();
};

#endif