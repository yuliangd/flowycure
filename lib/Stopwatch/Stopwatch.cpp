#include "Stopwatch.h"

Stopwatch::Stopwatch()
{
    this->timer_sec = 0;
    this->_status = 0;
};

void Stopwatch::start()
{
    this->started_time = millis();
    this->_status = 1;
};

unsigned long Stopwatch::getElapsedMs()
{
    if (this->isRunning())
        return millis() - this->started_time;
    else if (this->isPaused())
        return _paused_time;
    else
        return 0;
};

unsigned long Stopwatch::getElapsedSec()
{
    return (unsigned long)this->getElapsedMs() / 1000;
};

unsigned int Stopwatch::getElapsedMin()
{
    return (unsigned int)floor(this->getElapsedSec() / 60.0);
};

unsigned int Stopwatch::getElapsedHour()
{
    return (unsigned int)floor(this->getElapsedMin() / 60.0);
};

unsigned int Stopwatch::getElapsedDay()
{
    return (unsigned int)floor(this->getElapsedHour() / 24.0);
};

bool Stopwatch::checkForSec(unsigned long _s)
{
    return (this->getElapsedSec() >= _s);
};

void Stopwatch::setTimerSec(unsigned long _s)
{
    this->timer_sec = _s;
};

void Stopwatch::startTimerSec(unsigned long _s)
{
    this->setTimerSec(_s);
    this->start();
}

bool Stopwatch::isTimerDone()
{
    if (this->isRunning())
        return (this->checkForSec(this->timer_sec));
    else
        return false;
};

bool Stopwatch::isTimerPending()
{
    return ((this->isPaused() || this->isRunning()) && !this->isTimerDone());
}

void Stopwatch::pause()
{
    if (this->isRunning())
    {
        _paused_time = millis() - this->started_time;
        this->_status = 2;
    }
};

void Stopwatch::unpause()
{
    if (this->isPaused())
    {
        this->started_time = millis() - _paused_time;
        this->_status = 1;
    }
};

unsigned long int Stopwatch::getRemainingSec()
{
    if (!this->isTimerDone())
        return this->timer_sec - this->getElapsedSec();
    else
        return 0;
}

String Stopwatch::secToStr(unsigned long _s)
{
    String res = "";
    int _ss = _s % 60;
    int _m_acu = (_s - _ss) / 60;
    int _mm = _m_acu % 60;
    int _hh = (int)(_m_acu - _mm) / 60;
    if (_hh < 10)
    {
        res = " ";
    }
    else
    {
        res = "";
    }
    res += (String(_hh) + "h");
    if (_mm < 10)
    {
        res += " ";
    }
    res += (String(_mm) + "m");
    if (_ss < 10)
    {
        res += " ";
    }
    res += (String(_ss) + "s");
    return res;
}

String Stopwatch::getElapsedString()
{
    return this->secToStr(this->getElapsedSec());
}

String Stopwatch::getRemainingString()
{
    return this->secToStr(this->getRemainingSec());
}

void Stopwatch::reset()
{
    this->_status = 0;
}

bool Stopwatch::isStandby()
{
    return (this->_status == 0);
}

bool Stopwatch::isRunning()
{
    return (this->_status == 1);
}

bool Stopwatch::isPaused()
{
    return (this->_status == 2);
}

bool Stopwatch::checkPulse()
{
    if (this->isTimerDone())
    {
        this->start();
        return true;
    }
    else
        return false;
}

String Stopwatch::getStatusString()
{
    return this->status_strings[this->_status];
}