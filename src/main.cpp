/*
Main and only control program for the automated curing device.
(c) 2021 Yuliang Deng, Keirton Inc.
*/

#define DEBUG_BIT 0
#define FIRMWARE_VER "3.3.5" // Device ID

#if DEBUG_BIT == 1
#define DEFSERPRT(x) Serial.print(x)
#define DEFSERPLN(x) Serial.println(x)
#else
#define DEFSERPRT(x)
#define DEFSERPLN(x)
#endif

// Customized pin settings
#include "DefinePin.h"  // IO pin assignments
#include "DefineTRIG.h" // Trigger Events ID for the state machine

#define IOT_ENABLED 1

// #include <Arduino.h>
#include <Adafruit_NeoPixel.h>
#include <DHT.h> // Digital relative humidity & temperature sensor AM2302/DHT22
#include <JC_Button.h>
#include <Fsm.h> // arduino-fsm Library by Jon Black
#include <RotaryEncoder.h>
#include <Wire.h>
#include <Ticker.h>
#ifdef ARDUINO_ESP32_DEV // https://github.com/platformio/platform-espressif32/blob/develop/boards/az-delivery-devkit-v4.json
#include <EEPROM.h>
#define MQTT_ENABLED 1
#elif defined(ARDUINO_SAMD_NANO_33_IOT)
#include <FlashStorage.h> // To store timer and trigger point setting recipes. Nano has zero EEPROM :(
#endif

#if DFR_1602_LCD // defined in "DefinePin.h"
#include "DFRobot_LCD.h"
#else
#include <LiquidCrystal_I2C.h> // Use <LiquidCrystal_I2C.h> alternatively in case
#endif

// Customized library
#include "Stopwatch.h"
#include "Menulcd.h"
#include "LightRing.h"
#include "RecipeHandler.h"
#include "SlopeTracker.h"

#if IOT_ENABLED
#define BLYNK_PRINT Serial
#include "DefineIOT.h" // Misc IOT related setting, such as 'datastream' assignment
#include "arduino_secrets.h"
#include "ArduinoJson.h"
// #blynk & Wifi

#ifdef ARDUINO_SAMD_NANO_33_IOT
#include <SPI.h>
#include <WiFiNINA.h>
#include <BlynkSimpleWiFiNINA.h>
#include "WiFiManager_Lite.h"

typedef struct
{
  int magic;
  char ssid[32];
  char pass[64];
} WifiCredentials;

FlashStorage(stored_wifi_credentials, WifiCredentials);
WifiCredentials my_wifi_credentials;

int wifi_status = WL_IDLE_STATUS;
WiFiClient wifiClient;
WiFiServer ap_server(80);

WiFiManager_Lite myWiFiManager;

String ssid_str;
String pass_str;
bool new_wifi_saved = false;

#elif defined(ARDUINO_ESP32_DEV)
#include <WiFi.h>
#include <EspMQTTClient.h>
#include <WiFiManager.h> // https://github.com/tzapu/WiFiManager
#include <BlynkSimpleEsp32.h>

WiFiManager wm;

#if MQTT_ENABLED == 1
#include <EspMQTTClient.h>
EspMQTTClient mqtt_client(NULL, NULL, MQTT_BROKER, MQTT_USER, MQTT_PASS, BLYNK_DEVICE_NAME, 1883);
void send_recipe_mqtt();
void send_misc_mqtt();

void onConnectionEstablished()
{
  send_misc_mqtt(); // Publish IP address over MQTT
  send_recipe_mqtt();
}

const unsigned long mqtt_timer_int = 30000L;
void send_telemetry_mqtt();
Ticker timer_mqtt(send_telemetry_mqtt, mqtt_timer_int);
#endif

#endif

char auth[] = BLYNK_AUTH;
BlynkTimer iot_timer;

#endif

#if DFR_1602_LCD        // defined in "DefinePin.h"
DFRobot_LCD lcd(16, 2); // 16 characters and 2 lines of show.
#else
LiquidCrystal_I2C lcd(0x27, 16, 2); // default standard I2C LCD
#endif

Button button1(BUTTONPIN);
RotaryEncoder encoder(ROTARY_PIN_B, ROTARY_PIN_A, RotaryEncoder::LatchMode::FOUR3);

#define DHTTYPE DHT22 // DHT 22 (AM2302)
DHT dht(DHTPIN, DHTTYPE);

#define NUMPIXELS 7
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, LEDRINGPIN);
LightRing myLightRing(pixels, NUMPIXELS);
char device_id[12];

// Dev switches
bool ble_client_established = false,
     blynk_enabled = false;
unsigned int currentState;
bool rh_in_finish_zone;
bool long_pressed_fired = false;
bool lcd_pulse;
unsigned long last_button_time = 0; // for the double click evaluation
// Curing process parameters
int burp_count;
float temp_realtime, rh_realtime, rh_1s_mva, rh_rate, t_1s_mva;
int index_para_scroll = 0;
int encoder_movement;
int flash_init_mark = 173;

Stopwatch state_timer, session_timer, finish_zone_timer, lcd_timer;
Menulcd menu_root(100, 100);  // menu value 100, 200
Menulcd menu_config(210, 10); // menu value 210, 220, 230
int menu_pos_code = 100;      // Default starting menu position at 'START' of root menu

String stage_desc[5] = {"Standby", "Config",
                        "Sealing", "Burping",
                        "Completed"};

// Curing strategy Parameters (recipe, set-points)
int recipe_dof = 6; // First n of the curing variable is open for the tweak.
RecipeHandler currentRecipe(recipe_dof);
const uint8_t avg_sample_time = 250;                          // ms
const uint16_t trend_sample_time = 30000;                     // ms
SlopeTracker t_short_buffer(4, avg_sample_time / 60000.0);    // 4 data points, 0.25 second each
SlopeTracker rh_short_buffer(4, avg_sample_time / 60000.0);   // 4 data points, 0.25 second each
SlopeTracker rh_long_buffer(60, trend_sample_time / 60000.0); // 60 data points, 0.5 minutes each

void read_sensor();
void trend_push();
Ticker timer_measure(read_sensor, avg_sample_time);
Ticker timer_trend(trend_push, trend_sample_time);

typedef struct
{
  int magic;
  int setpoint[6];
} Recipe;

//----------------------------------------------------------
// Reserve a portion of flash memory to store the recipe parameters
#ifdef ARDUINO_SAMD_NANO_33_IOT
FlashStorage(fs_recipe, Recipe);
#elif defined(ARDUINO_ESP32_DEV)
#define EEPROM_SIZE sizeof(Recipe)
#endif

void save_recipe()
{
  Recipe _fs_recipe_cache;
  for (int i = 0; i < recipe_dof; i++)
  {
    _fs_recipe_cache.setpoint[i] = currentRecipe.getSetpoint(i);
  }
  _fs_recipe_cache.magic = flash_init_mark;
#ifdef ARDUINO_SAMD_NANO_33_IOT
  fs_recipe.write(_fs_recipe_cache);
#elif defined(ARDUINO_ESP32_DEV)
  EEPROM.put(0, _fs_recipe_cache);
  EEPROM.commit();
#endif

  if (Serial)
  {
    DEFSERPLN("Recipe saved to flash memory.");
    currentRecipe.printSerial();
  }
}

bool load_recipe()
{
  Recipe _fs_recipe_cache;
#ifdef ARDUINO_SAMD_NANO_33_IOT
  _fs_recipe_cache = fs_recipe.read();
#elif defined(ARDUINO_ESP32_DEV)
  EEPROM.get(0, _fs_recipe_cache);
  // placeholder for recipe handling in flash for ESP32 using EEPROM
#endif
  if (_fs_recipe_cache.magic == flash_init_mark)
  {
    for (int i = 0; i < recipe_dof; i++)
    {
      currentRecipe.setSetpoint(i, _fs_recipe_cache.setpoint[i]);
    }
    if (Serial)
    {
      DEFSERPLN("Recipe loaded from flash memory.");
      currentRecipe.printSerial();
    }
    return true;
  }
  else
    return false;
}
//----------------------------------------------------------
void stage_start_print()
{
  DEFSERPLN("Start - " + stage_desc[currentState]);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print(stage_desc[currentState]);
}
//----------------------------------------------------------
void stage_end_print()
{
  DEFSERPRT("End - " + stage_desc[currentState] + " ( ");
  DEFSERPRT(state_timer.getElapsedMin());
  DEFSERPLN(" min )");
}
//----------------------------------------------------------
void menu_handler(Menulcd &_m)
{
  if (encoder_movement != 0)
  {
    _m.movePos(encoder_movement);
    _m.updateLCD(lcd);
    menu_pos_code = _m.getPosCode();
  }
}
//----------------------------------------------------------
void lcd_curing_status(bool _enable)
{
  index_para_scroll = (index_para_scroll + encoder_movement + 10 * 5) % 5;
  if (encoder_movement != 0)
  {
    lcd.setCursor(0, 1);
    lcd.print("            ");
  }
  String msg[5];
  if (_enable || encoder_movement != 0)
  {
    msg[0] = state_timer.getRemainingString();
    msg[1] = String(session_timer.getElapsedDay()) + "D " + String(session_timer.getElapsedHour() % 24) + "H Total";
    msg[2] = String(burp_count) + " Burps";
    msg[3] = "DbClk->SKIP";
    msg[4] = "Hold->RESET";
    lcd.setCursor(0, 1);
    lcd.print(msg[index_para_scroll]);
  }
}
//--------------
void lcd_flash_msg(String _msg, bool _clear)
{
  lcd.setCursor(0, 1);
  lcd.print("[          ]");
  lcd.setCursor(1, 1);
  lcd.print(_msg);
  if (_clear)
  {
    delay(1000);
    lcd.setCursor(0, 1);
    lcd.print("            ");
  }
}
//----------------------------------------------------------
void lcd_t_rh()
{
  lcd.setCursor(12, 0);
  lcd.print(String((int)t_1s_mva) + String((char)223) + "C");
  lcd.setCursor(12, 1);
  lcd.print(String((int)rh_1s_mva) + " %");
} //----------------------------------------------------------
#if IOT_ENABLED
//----------------------------------------------------------
#ifdef ARDUINO_SAMD_NANO_33_IOT
void saveWiFiCredFlash()
{
  my_wifi_credentials.magic = flash_init_mark;
  ssid_str.toCharArray(my_wifi_credentials.ssid, 32);
  pass_str.toCharArray(my_wifi_credentials.pass, 64);
  stored_wifi_credentials.write(my_wifi_credentials);
}
void checkWiFiCredFlash()
{
  my_wifi_credentials = stored_wifi_credentials.read();
  if (my_wifi_credentials.magic != flash_init_mark)
  {
    ssid_str = SECRET_SSID;
    pass_str = SECRET_PASS;
    saveWiFiCredFlash();
  }
  else
  {
    ssid_str = String(my_wifi_credentials.ssid);
    pass_str = String(my_wifi_credentials.pass);
  }
}
#endif
//----------------------------------------------------------
void sendSensor() // #blynk
{
  if (WiFi.status() == WL_CONNECTED && (Blynk.connected()))
  {
    Blynk.virtualWrite(BLYNK_VPIN_RH, rh_1s_mva);
    Blynk.virtualWrite(BLYNK_VPIN_T, t_1s_mva);
    Blynk.virtualWrite(BLYNK_VPIN_PUMP, (int)digitalRead(PUMPPIN));
    if (rh_long_buffer.ready())
    {
      Blynk.virtualWrite(BLYNK_VPIN_DERIV_RH, rh_rate);
    }
  }
}
//----------------------------------------------------------
void wifi_iot_init()
{
  lcd_flash_msg("TryingWiFi", false);
  int wifi_ok;
#ifdef ARDUINO_SAMD_NANO_33_IOT
  wifi_ok = (myWiFiManager.connectWifi(ssid_str, pass_str) == WL_CONNECTED);
#elif defined(ARDUINO_ESP32_DEV)
  WiFi.mode(WIFI_STA); // explicitly set mode, esp defaults to STA+AP
  wm.setConfigPortalBlocking(false);
  wm.autoConnect("AutoConnectAP");
  delay(4000);
  wifi_ok = wm.autoConnect("AutoConnectAP");
#endif

  if (wifi_ok)
  {
    lcd_flash_msg("WiFi OK!", true);
    lcd_flash_msg("Trying IoT", true);
    Blynk.config(auth, IPAddress(64, 225, 16, 22), 8080);
    bool iot_res = Blynk.connect(); // #blynk
    if (iot_res)
    {
      lcd_flash_msg("IOT OK!", false);
      iot_timer.setInterval(30000L, sendSensor);
      blynk_enabled = true;
    }
    else
    {
      lcd_flash_msg("IOT OFF", false);
      blynk_enabled = false;
    }
  }
  else
  {
    lcd_flash_msg("WiFi Fail", true);
    blynk_enabled = false;
  }
}
void recipe_2_blynk()
{
  if (Blynk.connected())
  {
    Blynk.virtualWrite(BLYNK_VPIN_S_TIMER, currentRecipe.getSetpoint(0) * 60 - currentRecipe.getSetpoint(1));
    Blynk.virtualWrite(BLYNK_VPIN_S2B_RH, currentRecipe.getSetpoint(2));
    Blynk.virtualWrite(BLYNK_VPIN_B_TIMER, currentRecipe.getSetpoint(1));
    Blynk.virtualWrite(BLYNK_VPIN_B2S_RH, currentRecipe.getSetpoint(3));
    Blynk.virtualWrite(BLYNK_VPIN_DONE_RH, currentRecipe.getSetpoint(4));
    Blynk.virtualWrite(BLYNK_VPIN_CHK_DUR, currentRecipe.getSetpoint(5));
  }
}
#endif
//----------------------------------------------------------
//---------------- STATE MACHINE DECLARATION START----------
//----------------------------------------------------------
void on_standby_enter()
{
  currentState = 0;
  menu_root.setPosition(0);
  menu_pos_code = menu_root.getPosCode(); // default menu position (Start)
  menu_root.updateLCD(lcd);
};
//--------------
void on_standby_state()
{
  menu_handler(menu_root);
}
//--------------
void on_standby_exit()
{
  lcd.clear();
  menu_config.setPosition(0);
  currentRecipe.initAllTemp();
}
//--------------
void on_selvar_enter()
{
  currentState = 1;
  menu_pos_code = menu_config.getPosCode();
  for (int i = 0; i < recipe_dof; i++)
  {
    String newline = currentRecipe.getDescValueUnit(i);
    menu_config.updateItem(i + 1, newline);
  }
  menu_config.updateLCD(lcd);
}
//--------------
void on_selvar_state()
{
  if (menu_pos_code % 10 == 0)
  {
    menu_handler(menu_config);
  }
}
//--------------
void on_selvar_exit()
{
}
//--------------
void on_wifisetup_enter()
{
  currentState = 1;
  menu_pos_code = 310;
#if IOT_ENABLED & defined(ARDUINO_SAMD_NANO_33_IOT)
  new_wifi_saved = false;
  char ap_ssid[] = AP_SSID;
  int status = WiFi.beginAP(ap_ssid);
  ap_server.begin();
  if (status == WL_AP_LISTENING)
  {
    String ap_ip = "192.168.4.1";
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("SSID:" + String(ap_ssid));
    lcd.setCursor(0, 1);
    lcd.print(ap_ip);
  }
#endif
}

void on_wifisetup_state()
{
#if IOT_ENABLED & defined(ARDUINO_SAMD_NANO_33_IOT)
  myWiFiManager.httpHandler(ap_server);
  if (myWiFiManager.isNewCredentialReady() && !new_wifi_saved)
  {
    ssid_str = myWiFiManager.getNewSsid();
    pass_str = myWiFiManager.getNewPass();
    saveWiFiCredFlash();
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("SSID:" + ssid_str);
    lcd_flash_msg("WiFi Set!", true);
    new_wifi_saved = true;
    // resetFunc();
  }
#endif
}

void on_wifisetup_exit()
{
#if IOT_ENABLED & defined(ARDUINO_SAMD_NANO_33_IOT)
  lcd_flash_msg("Canceled", true);
#endif
}
//--------------
void tran_curing_start()
{
  burp_count = 0;
  session_timer.start();
  rh_in_finish_zone = false; // clear the finish flag
#if IOT_ENABLED
  if (Blynk.connected())
    Blynk.logEvent("started");
#endif
}
void tran_apply_new_curvar()
{
  // apply and save the recipe
  currentRecipe.applyAllTemp();
  save_recipe();
  lcd_flash_msg("SAVED", true);
#if IOT_ENABLED
  recipe_2_blynk();
#if MQTT_ENABLED == 1
  send_recipe_mqtt();
#endif
#endif
}
//--------------
void disp_curval()
{
  lcd.setCursor(5, 1);
  int i = menu_config.getPosition() - 1;
  char _unit = currentRecipe.getUnit(i);
  int _spTemp = currentRecipe.getSpTemporary(i);
  if (_unit == 'm')
  {
    lcd.print(state_timer.secToStr(_spTemp * 60UL).substring(0, 6));
  }
  else
  {
    lcd.print(_spTemp);
    lcd.print(" ");
    lcd.print(_unit);
    lcd.print(" "); // clear out the following space, in case goes from 100 -> 99, or 10 ->9
  }
}
//--------------
void on_change_enter()
{
  lcd.setCursor(0, 0);
  lcd.print(" ");
  lcd.setCursor(0, 1);
  lcd.print("                ");
  lcd.setCursor(3, 1);
  lcd.print(char(255));
  disp_curval();
}
//--------------
void on_change_state()
{
  int i = menu_config.getPosition() - 1;
  lcd.setCursor(1, 1);
  if (encoder_movement != 0)
  {
    currentRecipe.changeSpTemporary(i, encoder_movement);
    disp_curval();
  }
}
//--------------
void on_change_exit() {}
//--------------
void on_seal_enter()
{
  currentState = 2;
  menu_pos_code = 0;
  index_para_scroll = 0;
  stage_start_print();
  state_timer.startTimerSec((currentRecipe.getSetpoint(0) * 60UL - currentRecipe.getSetpoint(1)) * 60UL);
  rh_long_buffer.reset();
};
//--------------
void on_seal_state()
{
  lcd_curing_status(lcd_pulse);
}
//--------------
void on_seal_exit()
{
  stage_end_print();
};
//--------------
void on_burp_enter()
{
  currentState = 3;
  index_para_scroll = 0;
  stage_start_print();
  state_timer.startTimerSec(currentRecipe.getSetpoint(1) * 60UL);
  rh_long_buffer.reset();
  burp_count++;
  digitalWrite(PUMPPIN, HIGH); // ON
};
//--------------
void on_burp_state()
{
  lcd_curing_status(lcd_pulse);
}
//--------------
void on_burp_exit()
{
  digitalWrite(PUMPPIN, LOW); // OFF
  stage_end_print();
};
//--------------
void on_completed_enter()
{
  currentState = 4;
  stage_start_print();
  lcd_flash_msg("COMPLETED", true);
#if IOT_ENABLED
  if (Blynk.connected())
    Blynk.logEvent("completed");
#endif
};
//--------------
void on_completed_state()
{
  // placeholder;
}
//----------------------------------------------------------
// State Machine Declaration
State state_standby(&on_standby_enter, &on_standby_state, &on_standby_exit);
State state_selvar(&on_selvar_enter, &on_selvar_state, &on_selvar_exit);
State state_change(&on_change_enter, &on_change_state, &on_change_exit);
State state_wifisetup(&on_wifisetup_enter, &on_wifisetup_state, &on_wifisetup_exit);
State state_seal(&on_seal_enter, &on_seal_state, &on_seal_exit);
State state_burp(&on_burp_enter, &on_burp_state, &on_burp_exit);
State state_completed(&on_completed_enter, &on_completed_state, NULL);
Fsm fsm_flowycure(&state_standby);
//----------------------------------------------------------
void checkTimerAndTrig(Fsm &_fsm)
{
  if (state_timer.isTimerDone())
    _fsm.trigger(TIMER_DONE_EVENT);
};
//----------------------------------------------------------
void checkButtonsAndTrig()
{
  if (button1.wasPressed())
  {
    DEFSERPLN("Button Pressed"); // debugging
    if ((currentState < 4) & (menu_pos_code % 100 == 0 || (menu_pos_code == MENU_CONFIG_CANCEL_EVENT || menu_pos_code == MENU_CONFIG_OK_EVENT)))
    {
      fsm_flowycure.trigger(menu_pos_code);
    }
    else
    {
      fsm_flowycure.trigger(BUTTON_PRESSED_EVENT);
    }
    long_pressed_fired = false;
    if (button1.lastChange() - last_button_time < 789)
      fsm_flowycure.trigger(DBL_CLICK_EVENT);
    else
      last_button_time = button1.lastChange();
  }
  if (button1.pressedFor(1600)) // Going to tap mode
  {
    if (!long_pressed_fired)
    {
      fsm_flowycure.trigger(BUTTON_LONGPRESS_EVENT);
      long_pressed_fired = true;
    }
  }
}
//----------------------------------------------------------
void checkCompletionAndTrig()
{
  // RH lower than done threashold & In the sealing state only (not any other state)
  bool res = ((int)rh_1s_mva < currentRecipe.getSetpoint(4) && (currentState == 2));
  if (res)
  {
    if (!rh_in_finish_zone)
    {
      finish_zone_timer.startTimerSec(currentRecipe.getSetpoint(5) * 60L);
      rh_in_finish_zone = true;
    }
  }
  else
  {
    rh_in_finish_zone = false;
    finish_zone_timer.reset();
  }

  bool rh_stable = (rh_in_finish_zone && finish_zone_timer.isTimerDone());
  const float rh_slope_threshold = 0.03; // change rate < %RH per hour to declare finish
  rh_stable = (rh_stable && ((abs(rh_rate) <= rh_slope_threshold)) && rh_long_buffer.ready());
  if (rh_stable || session_timer.isTimerDone())
  {
    // session_timer.pause(); // Freeze the session timer upon finish
    fsm_flowycure.trigger(COMPLETION_EVENT);
  }
}

//----------------------------------------------------------
void init_menu()
{
  menu_root.addItem("START");
  menu_root.addItem("SETTINGS");
#if IOT_ENABLED
  if (WiFi.status() != WL_CONNECTED)
  {
    menu_root.addItem("WIFI SETUP");
  }
#endif
  menu_config.addItem(String(char(0b01111111)) + " BACK");
  for (int i = 0; i < recipe_dof; i++)
  {
    menu_config.addItem(currentRecipe.getDesc(i));
  }
  menu_config.addItem(String(char(0b01111111)) + " SAVE");
}

void encoder_tick()
{
  encoder.tick();
}

void encoder_check()
{
  encoder_movement = (int)encoder.getDirection();
  unsigned int delta_t = encoder.getMillisBetweenRotations();
  if (delta_t < 160)
  {
    encoder_movement *= 2;
    if (delta_t < 75)
    {
      encoder_movement *= 3;
    }
  }
  if (delta_t < 30)
  {
    encoder_movement = 0;
  }
}

bool power_on_self_test()
{
  delay(1600); // if starts too soon the Serial monitor won't catch the message.
  DEFSERPLN("POWER-ON-SELF-TEST");

  DEFSERPRT("Sensor Check: ");
  float _t = dht.readTemperature(); // read_sensor
  float _rh = dht.readHumidity();
  bool sensor_ok = (_t > -10.0 && _t < 80.0) && (_rh > 0.0 && _rh < 100.0);
  if (sensor_ok)
    DEFSERPRT("OK");
  else
    DEFSERPRT("Reading out of range -");
  DEFSERPLN(String(_t) + char(223) + "C, /" + String(_rh) + "% RH");

  DEFSERPRT("Button Check: ");
  bool pinread_button = digitalRead(BUTTONPIN);
  bool button_ok = (pinread_button == HIGH);
  if (button_ok)
    DEFSERPLN("OK");
  else
    DEFSERPLN("Fail or Being depressed?");

  DEFSERPRT("Encoder Check: ");
  bool pinread_enc_a = digitalRead(ROTARY_PIN_A);
  bool pinread_enc_b = digitalRead(ROTARY_PIN_B);
  bool encoder_ok = (pinread_enc_a == HIGH && pinread_enc_b == HIGH);
  if (encoder_ok)
    DEFSERPLN("OK");
  else
  {
    DEFSERPRT("Fail or not parked on indent? ");
    DEFSERPLN(String(pinread_enc_a) + String(pinread_enc_b));
  }

  return (sensor_ok && button_ok && encoder_ok);
}
//----------------------------------------------------------
void setup()
{
  pinMode(PUMPPIN, OUTPUT);
  digitalWrite(PUMPPIN, LOW);

#ifdef ARDUINO_ESP32_DEV
  EEPROM.begin(sizeof(Recipe));
  sprintf(device_id, "CRPK-%06X", (uint32_t)(ESP.getEfuseMac() & 0xFFFFFF));

#endif
  Serial.begin(9600);
  attachInterrupt(digitalPinToInterrupt(ROTARY_PIN_A), encoder_tick, CHANGE);
  attachInterrupt(digitalPinToInterrupt(ROTARY_PIN_B), encoder_tick, CHANGE);
  button1.begin();

  dht.begin();
  myLightRing.begin();

  lcd.init();     // initialize the lcd. lcd.backlight() not needed for the DFRobot LCD
#if !DFR_1602_LCD // defined in "DefinePin.h"
  lcd.backlight();
#endif

  lcd.print("<< Cure-Puck >>"); // Welcome message to the LCD.
  lcd.setCursor(2, 1);
  lcd.print(FIRMWARE_VER);
  delay(1000);
  lcd.setCursor(2, 1);
  lcd.print("By Keirton");
  lcd_timer.startTimerSec(1);
  session_timer.setTimerSec(currentRecipe.getSetpoint(6) * 24UL * 3600UL); // set max curing days

  if (power_on_self_test())
    myLightRing.showRotate();
  else
    myLightRing.showColorFlash(0xFF0000, 500);

  if (!load_recipe())
  {
    save_recipe();
  }

#if IOT_ENABLED
#ifdef ARDUINO_SAMD_NANO_33_IOT
  checkWiFiCredFlash();
#else
#if MQTT_ENABLED == 1
  mqtt_client.enableHTTPWebUpdater(); // Enable the web updater. User and password default to values of MQTTUsername and MQTTPassword.
  mqtt_client.enableLastWillMessage("dt/flowycure/logs/lastwill", BLYNK_DEVICE_NAME);
  timer_mqtt.start();
#endif
#endif
  wifi_iot_init();
  recipe_2_blynk();
#endif

  fsm_flowycure.add_transition(&state_standby, &state_seal, MENU_START_CURING_EVENT, &tran_curing_start);
  fsm_flowycure.add_transition(&state_standby, &state_selvar, MENU_TO_CONFIG_EVENT, NULL);
  fsm_flowycure.add_transition(&state_standby, &state_wifisetup, MENU_TO_WIFI_EVENT, NULL);
  fsm_flowycure.add_transition(&state_wifisetup, &state_standby, NEW_WIFI_SET_EVENT, NULL);
  fsm_flowycure.add_transition(&state_wifisetup, &state_standby, BUTTON_LONGPRESS_EVENT, NULL);
  fsm_flowycure.add_transition(&state_selvar, &state_change, BUTTON_PRESSED_EVENT, NULL);
  fsm_flowycure.add_transition(&state_change, &state_selvar, BUTTON_PRESSED_EVENT, NULL);
  fsm_flowycure.add_transition(&state_selvar, &state_standby, MENU_CONFIG_OK_EVENT, &tran_apply_new_curvar);
  fsm_flowycure.add_transition(&state_selvar, &state_standby, MENU_CONFIG_CANCEL_EVENT, NULL);
  fsm_flowycure.add_transition(&state_seal, &state_burp, TIMER_DONE_EVENT, NULL);
  fsm_flowycure.add_transition(&state_seal, &state_burp, RH_OVER_EVENT, NULL);
  fsm_flowycure.add_transition(&state_burp, &state_seal, TIMER_DONE_EVENT, NULL);
  fsm_flowycure.add_transition(&state_burp, &state_seal, RH_BELOW_EVENT, NULL);
  fsm_flowycure.add_transition(&state_seal, &state_completed, COMPLETION_EVENT, NULL);
  fsm_flowycure.add_transition(&state_completed, &state_standby, BUTTON_PRESSED_EVENT, NULL);
  fsm_flowycure.add_transition(&state_completed, &state_standby, BUTTON_LONGPRESS_EVENT, NULL);
  fsm_flowycure.add_transition(&state_seal, &state_standby, BUTTON_LONGPRESS_EVENT, NULL);
  fsm_flowycure.add_transition(&state_burp, &state_standby, BUTTON_LONGPRESS_EVENT, NULL);
  fsm_flowycure.add_transition(&state_seal, &state_burp, DBL_CLICK_EVENT, NULL);
  fsm_flowycure.add_transition(&state_burp, &state_seal, DBL_CLICK_EVENT, NULL);

  init_menu();
  encoder.tick();
  encoder_movement = 0;
  timer_measure.start();
  timer_trend.start();

  if (Serial)
    DEFSERPLN("Setup loop completed");
}

//----------------------------------------------------------
void loop()
{
  button1.read(); // Taken over by interrupt
  checkButtonsAndTrig();
  encoder_check(); // Taken over by interrupt

  fsm_flowycure.run_machine();
  timer_measure.update();
  timer_trend.update();

  checkTimerAndTrig(fsm_flowycure);
  checkCompletionAndTrig();

  if ((((int)rh_1s_mva > currentRecipe.getSetpoint(2)) && state_timer.getElapsedMin() > 5) && currentState == 2)
  {
    fsm_flowycure.trigger(RH_OVER_EVENT);
  }
  if ((((int)rh_1s_mva < currentRecipe.getSetpoint(3)) && !rh_in_finish_zone) && currentState == 3)
  {
    fsm_flowycure.trigger(RH_BELOW_EVENT);
  }
  //

#if IOT_ENABLED
  if (blynk_enabled && (WiFi.status() == WL_CONNECTED)) // #blynk
  {
    Blynk.run();
    iot_timer.run();
  }
#ifdef ARDUINO_SAMD_NANO_33_IOT
  if (new_wifi_saved)
  {
    fsm_flowycure.trigger(NEW_WIFI_SET_EVENT);
    wifi_iot_init();
    new_wifi_saved = false;
  }
#elif defined(ARDUINO_ESP32_DEV)
  wm.process();
#if MQTT_ENABLED == 1
  mqtt_client.loop();
  timer_mqtt.update();
#endif
#endif
#endif
  // LED ring indicator
  myLightRing.showPattern(currentState, (int)rh_1s_mva);
  lcd_pulse = lcd_timer.checkPulse();
  if (lcd_pulse && (currentState != 1))
    lcd_t_rh(); // Update LCD cyclically unless it's in the 'Setting' mode
  // delay(5);
}

void read_sensor()
{
  temp_realtime = dht.readTemperature(); // read_sensor
  rh_realtime = dht.readHumidity();      // read_sensor
  rh_short_buffer.addPoint(rh_realtime);
  t_short_buffer.addPoint(temp_realtime);
  if (rh_short_buffer.ready())
  {
    rh_1s_mva = rh_short_buffer.getAvg();
  }
  else
  {
    rh_1s_mva = rh_realtime;
  }
  if (t_short_buffer.ready())
  {
    t_1s_mva = t_short_buffer.getAvg();
  }
  else
  {
    t_1s_mva = temp_realtime;
  }
}

void trend_push()
{
  rh_long_buffer.addPoint(rh_1s_mva);
  if (rh_long_buffer.ready())
  {
    rh_rate = rh_long_buffer.getSlope();
  }
  else
  {
    rh_rate = 0;
  }
}

#if MQTT_ENABLED == 1
void send_telemetry_mqtt()
{
  String tele_topic = MQTT_TOPIC_PREFIX;
  tele_topic += device_id;
  tele_topic += MQTT_TOPIC_SUFFIX;

  DynamicJsonDocument tele_json(128);
  tele_json["t"] = t_1s_mva;
  tele_json["h"] = rh_1s_mva;
  tele_json["state"] = stage_desc[currentState];
  tele_json["dev_id"] = device_id;
  char json_out[128];
  int b = serializeJson(tele_json, json_out);

  mqtt_client.publish(tele_topic, json_out);
}

void send_recipe_mqtt()
{
  String tele_topic = MQTT_TOPIC_PREFIX;
  tele_topic += BLYNK_DEVICE_NAME;
  tele_topic += "/recipe";

  DynamicJsonDocument tele_json(256);
  for (int i = 0; i < recipe_dof; i++)
  {
    tele_json[currentRecipe.getDesc(i)] = currentRecipe.getSetpoint(i);
  }
  char json_out[256];
  int b = serializeJson(tele_json, json_out);

  mqtt_client.publish(tele_topic, json_out);
}

void send_misc_mqtt()
{
  String topic = MQTT_TOPIC_PREFIX;
  topic += BLYNK_DEVICE_NAME;
  topic += "/misc";
  DynamicJsonDocument misc_json(64);
  misc_json["ip"] = WiFi.localIP().toString();
  misc_json["version"] = FIRMWARE_VER;
  char json_out[64];
  int b = serializeJson(misc_json, json_out);
  mqtt_client.publish(topic, json_out);
}
#endif