// BLE Device AND UUID assignment
#define BLEDEVICENAME "FlowyCure_001"
// Environment sensing service UUID
// https://developer.bluetooth.org/gatt/services/Pages/ServiceViewer.aspx?u=org.bluetooth.service.environmental_sensing.xml
#define SERVICE_UUID_ESS "181A"
#define CHAR_UUID_TEMPERATURE "2A6E"                            // Temperature characteristic UUID
#define CHAR_UUID_HUMIDITY "2A6F"                               // Humidity characteristic UUID

#define SERVICE_UUID_CUR "05b0beed-7c4c-48ac-b725-e7a6a5a699c7" // Curing parameters Service UUID
// Curing method setpoints
#define CHAR_UUID_TSEAL "e687f81f-dbc8-41d6-81a7-51681e661df0"    // Timer for sealing characteristic UUID
#define CHAR_UUID_TBURP "127734b2-0f18-4f4b-9d9d-f454e8f802ff"    // Timer for burping characteristic UUID
#define CHAR_UUID_RH2 "49979d68-7121-4889-b26f-cf93911323dc"    // RH Trigger to start burping
#define CHAR_UUID_RH3 "ac66fe36-9616-4360-9982-7caa9883afc7"    // RH Trigger to stop burping
#define CHAR_UUID_RHTU "cb43e840-0d0c-4c17-a079-4164877763cd" // RH_target_upper
#define CHAR_UUID_RHTL "88c1f3fd-6a60-4298-b364-0b8268299be0" // RH_target_lower
#define CHAR_UUID_TIMERCOMP "8a5b11cb-bb47-4d4f-a14d-964e4ef53600" // RH_stable_duration as completion condition
#define CHAR_UUID_MAXDAYS "e4163a29-8a40-4909-a6a1-f79db46467ec" // Max_curing_days

// Curring running informations
#define CHAR_UUID_CURETIME "2B07" // Relative Runtime In A Current Range / Total Curing Time in Minutes
#define CHAR_UUID_STAGE "c88cc833-4472-4707-a891-e01b03e42f10" // Current Device Stage