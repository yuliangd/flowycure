// IO pin assignments

#if defined(ARDUINO_SAMD_NANO_33_IOT) || defined(SEEED_XIAO_M0)
#define PUMPPIN 2    // to turn on the comporessor, through a relay
#define LEDRINGPIN 3 // input pin of the LED ring
#define BUTTONPIN 11  // a N.O. pushbutton between this and GND. Int Pull-up resistor to be used. (was 6)
#define DHTPIN 7     // Temperature & humidity sensor
#define ROTARY_PIN_A 10 // Channal A input pin for rotary encoder. (was 8)
#define ROTARY_PIN_B 9 // Channal B input pin for rotary encoder
#endif

#ifdef ARDUINO_ESP32_DEV
#define PUMPPIN 4    // to turn on the comporessor, through a relay.
#define LEDRINGPIN 12 // input pin of the LED ring
#define BUTTONPIN 33  // a N.O. pushbutton between this and GND. Int Pull-up resistor to be used.
#define DHTPIN 2     // Temperature & humidity sensor
#define ROTARY_PIN_A 26 // Channal A input pin for rotary encoder
#define ROTARY_PIN_B 25 // Channal B input pin for rotary encoder
#endif

#define DFR_1602_LCD 1 // comment out or set to 0 if using standard I2C LCD